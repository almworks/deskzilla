package com.almworks.bugzilla.integration.oper;

public enum BugzillaAuthType {
  CGI,
  ENV
}
