package com.almworks.database.objects;

import com.almworks.api.database.Revision;

/**
 * :todoc:
 *
 * @author sereda
 */
public interface RevisionWithInternals extends Revision, RevisionInternals {
}
