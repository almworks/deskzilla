package com.almworks.database;

/**
 * :todoc:
 *
 * @author sereda
 */
public class RuntimeDatabaseInconsistentException extends RuntimeException {
  public RuntimeDatabaseInconsistentException(Throwable cause) {
    super(cause);
  }
}
