package com.almworks.database;

/**
 * :todoc:
 *
 * @author sereda
 */
public class TransactionControlException extends RuntimeException {
  public TransactionControlException(String message) {
    super(message);
  }
}
