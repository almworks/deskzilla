package com.almworks.api.database;

/**
 * :todoc:
 *
 * @author sereda
 */
public class UnsafeCollisionException extends RuntimeException {
  public UnsafeCollisionException(Throwable cause) {
    super(cause);
  }
}
