package com.almworks.api.database;

public class CollisionException extends Exception {
  public CollisionException(Throwable cause) {
    super(cause);
  }
}
