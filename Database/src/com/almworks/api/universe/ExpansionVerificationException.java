package com.almworks.api.universe;

/**
 * :todoc:
 *
 * @author sereda
 */
public class ExpansionVerificationException extends Exception {
  public ExpansionVerificationException() {
    super();
  }

  public ExpansionVerificationException(Throwable cause) {
    super(cause);
  }

  public ExpansionVerificationException(String message) {
    super(message);
  }

  public ExpansionVerificationException(String message, Throwable cause) {
    super(message, cause);
  }
}
