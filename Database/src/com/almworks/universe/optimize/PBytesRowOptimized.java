package com.almworks.universe.optimize;

import com.almworks.api.universe.Particle;

abstract class PBytesRowOptimized extends Particle {
  public abstract long getKey();
}
