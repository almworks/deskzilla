package com.almworks.platform;

class ProductInfoProperties {
  private final String version = "2.0.2";
  private final String buildNumber = "9999";
  private final String versionType = "EAP";
  private final String productName = "Tracker2";
  private final String buildDate = "2009/08/25 18:25 MSK";

  public String getVersion() {
    return version;
  }

  public String getBuildNumber() {
    return buildNumber;
  }

  public String getVersionType() {
    return versionType;
  }

  public String getProductName() {
    return productName;
  }

  public String getBuildDate() {
    return buildDate;
  }
}