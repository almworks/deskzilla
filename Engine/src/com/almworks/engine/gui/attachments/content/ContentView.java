package com.almworks.engine.gui.attachments.content;

import javax.swing.*;

public interface ContentView {

  void copyToClipboard();

  JComponent getComponent();
}
