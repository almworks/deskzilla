package com.almworks.util.commons;

/**
 * @author dyoma
 */
public interface Procedure2<A, B> {
  void invoke(A a, B b);
}
